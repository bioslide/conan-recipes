# Conan file for Jpeg XR codec - a library for decompressiong of jpeg xr images
### to create Release package:
```
conan create . slideio/stable -s build_type=Release
```
### to create Debug package:
```
conan create . slideio/stable -s build_type=Debug
```
### Upload packages to conan server
```
conan upload "jpegxrcodec/1.0.0@slideio/stable" -r slideio --all
```