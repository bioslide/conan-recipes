from conans import ConanFile, CMake, tools
import os
import glob


class PoleConan(ConanFile):
    name = "pole"
    url = ""
    version = "1.0.4"
    description = "portable library for structured storagec."
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"
    license = "BSD 3-Clause"
    _source_subfolder = "source_subfolder"

    def config_options(self):
        if self.settings.os == "Windows":
            self.options.remove("fPIC")

    def source(self):
        git = tools.Git()
        git.clone("https://gitlab.com/bioslide/pole.git", self.version, args="--recursive")

    def _configure_cmake(self):
        cmake = CMake(self)
        if self.settings.os != "Windows":
           cmake.definitions['CMAKE_POSITION_INDEPENDENT_CODE'] = True
        cmake.definitions['CMAKE_CXX_STANDARD'] = 17
        cmake.configure()
        return cmake

    def build(self):
        # ensure that bundled cmake files are not used
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        self.copy("*", dst="include/pole", src="install/includes")
        self.copy("*", dst="lib", src="install/lib", keep_path=False)

    def package_info(self):
        self.cpp_info.includedirs = ["include"]
        self.cpp_info.libs = ["pole"]
        self.cpp_info.libdirs = ["lib"]
