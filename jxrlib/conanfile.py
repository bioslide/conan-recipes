from conans import ConanFile, CMake, tools
import os
import glob

class JxrlibConan(ConanFile):
    name = "jxrlib"
    url = ""
    version = "1.0.0"
    description = "Jpeg XR codec."
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"
    license = "BSD 2-Clause"
    _source_subfolder = "source_subfolder"

    def config_options(self):
        if self.settings.os == "Windows":
            self.options.remove("fPIC")

    def source(self):
        git = tools.Git()
        git.clone("https://gitlab.com/bioslide/jpegxrcodec.git", "v{0}".format(self.version), args="--recursive")

    def _configure_cmake(self):
        cmake = CMake(self)
        if self.settings.os != "Windows":
           cmake.definitions['CMAKE_POSITION_INDEPENDENT_CODE'] = True
        cmake.configure()
        return cmake

    def build(self):
        # ensure that bundled cmake files are not used
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        cmake = self._configure_cmake()
        cmake.install()
        if self.settings.os == "Macos":
            self.copy("*.a", dst="lib", src=os.path.join(str(self.settings.build_type), "bin"))

    def package_info(self):
        if self.settings.os == "Macos":
            self.cpp_info.libs = ["libjxrcodec.a"]
        else:
            self.cpp_info.libs = tools.collect_libs(self)
        self.cpp_info.includedirs = ["include/jxrlib"]
