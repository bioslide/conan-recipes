# boost-slideio
it is a copy of conan-center with a change that disables dependency
from statx for file-system in case if compiler.libcxx is set to "libstdc++".
It is needed for manylinux system

### to create package run (from the package directory):
```
conan create . boost/1.81.0@slideio/stable -pr path-to-profile
```