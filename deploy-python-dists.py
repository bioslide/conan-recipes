import glob
import os
import sys
import json


def get_python_folders():
    dir = os.path.dirname(os.path.abspath(__file__))
    expr = dir + '/python-*'
    python_dirs = glob.glob(expr)
    return python_dirs


def get_default_path_file():
    dir = os.path.dirname(os.path.abspath(__file__))
    return os.path.join(dir, "py.txt")


def deploy_python_dist(dist_folder, target_file):
    python_bin = 'bin/python'
    if os.name == 'nt':
        python_bin = 'python.exe'
    file = os.path.join(dist_folder, "py.json")
    command = f"conan create -j {file} {dist_folder} slideio/stable -s build_type=Release"
    code = os.system(command)
    if code != 0:
        raise Exception(f"Error python deployment {folder}")
    f = open(file)
    py_props = json.load(f)
    py_path_root = py_props['installed'][0]['packages'][0]['cpp_info']['rootpath']
    py_path_bin = py_props['installed'][0]['packages'][0]['cpp_info']['bindirs'][0]
    py_path = os.path.join(py_path_root, py_path_bin, python_bin)
    with open(target_file, "a") as tfile:
        tfile.write(py_path)
        tfile.write('\n')
    print(f"Python {folder} successfully deployed")


if __name__ == "__main__":
    arguments = sys.argv[1:]
    path_file = get_default_path_file()
    if len(arguments) > 0:
        path_file = arguments[0]
    if os.path.exists(path_file):
        os.remove(path_file)

    folders = get_python_folders()

    for folder in folders:
        deploy_python_dist(folder, path_file)
