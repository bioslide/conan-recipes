# conan-recipes
Build order:
- sqlite
- proj
- libtiff
- gdal
- opencv

No dependencies (any order):
- openjpeg

### to create Debug package (from the package directory):
```
conan create . slideio/stable -s build_type=Release
```
### to create Debug package:
```
conan create . slideio/stable -s build_type=Debug
```
### Upload packages to conan server
```
conan upload "jpegxrcodec/1.0.0@slideio/stable" -r slideio --all
```