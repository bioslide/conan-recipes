from conans import ConanFile, CMake, tools
import os
import glob


class OpenCVConan(ConanFile):
    name = "opencv-slideio"
    url = ""
    version = "4.10.3"
    description = "Opencv updated core for slideio."
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"
    license = "BSD 3-Clause"
    _source_subfolder = "source_subfolder"

    def config_options(self):
        if self.settings.os == "Windows":
            self.options.remove("fPIC")

    def source(self):
        git = tools.Git()
        git.clone("https://github.com/Booritas/opencv.git", "slideio", args="--recursive", shallow=True)

    def _configure_cmake(self):
        if self.settings.os == "Macos":
            cmake = CMake(self, generator="Xcode")
        else:
            cmake = CMake(self)
        if self.settings.os != "Windows":
           cmake.definitions['CMAKE_POSITION_INDEPENDENT_CODE'] = True
        cmake.configure(build_folder="build")
        return cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()
        cmake.install()

    def package(self):
        if self.settings.os != "Windows":
            self.copy("*", dst="bin", src="build/lib", keep_path=False)

    def package_info(self):
        self.cpp_info.includedirs = ["include"]
        self.cpp_info.libs = tools.collect_libs(self)
        self.cpp_info.libdirs = ["lib"]
        self.cpp_info.bindirs = ["bin"]
