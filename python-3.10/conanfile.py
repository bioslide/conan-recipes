from conans import ConanFile, CMake, tools
import os


class PythonConan(ConanFile):
    name = "python"
    license = "MIT"
    settings = "os", "arch"
    description = "Python 3 distributives."
    exports_sources = "src/*"
    version = "3.10"

    def build(self):
        vrs = self.version.split('.')
        suffix = vrs[0]  + vrs[1]
        zipfile_suffix = suffix
        if self.settings.os == "Macos":
            zipfile_suffix = zipfile_suffix + "-macos"

        # Detecting OS arm architecture
        if self.settings.arch == "armv8":
            zipfile_suffix += "-arm"

        file_name = "Python" + zipfile_suffix + ".zip"
        self.source_zip = os.path.join("src",file_name)
        self.folder = "Python" + suffix
        self.python_name = "python" + suffix
        tools.unzip(self.source_zip, keep_permissions=True)

    def package(self):
        self.copy("*", dst=self.folder, src=self.folder)

    def package_info(self):
        vrs = self.version.split('.')
        suffix = vrs[0]  + vrs[1]
        self.folder = "Python" + suffix
        self.python_name = "python" + suffix
        self.cpp_info.includedirs = [os.path.join(self.folder,"include")]
        self.cpp_info.libs = [self.python_name, self.python_name + "_d"]
        self.cpp_info.libdirs = [os.path.join(self.folder,"libs")]
        self.cpp_info.bindirs = [self.folder, os.path.join(self.folder,"Scripts")]