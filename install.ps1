# $packages = @("dcmtk","jpegxrcodec","pole", "gtest")
# for($p=0; $p -lt $packages.Length; $p++)
# {
#    $folder= $packages[$p]
#    & cd $folder
#    & conan create . slideio/stable -s build_type=Release
#    & cd ..
# }
New-Item -Name pythons.txt -ItemType File -Force
$pythons = @("3.9","3.8","3.7","3.6","3.5")
for($p=0; $p -lt $pythons.Length; $p++)
{
   $folder= "python-" +  $pythons[$p]
   & cd $folder
   & conan create -j py.json . slideio/stable -s build_type=Release
   $json = Get-Content -Raw -Path py.json | ConvertFrom-Json
   $pbin = Join-Path -Path $json.installed[0].packages[0].cpp_info.rootpath -ChildPath $json.installed[0].packages[0].cpp_info.bindirs[0] | Join-Path -ChildPath "python.exe"
   & echo $pbin
   & cd ..
   Add-Content -Path pythons.txt -Value $pbin
}